import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CovidComponent } from './covid/covid.component';
import { DasboardComponent } from './dasboard/dasboard.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'home', component: HomeComponent},
  { path: 'estadisticas', component: CovidComponent},
  { path: 'graficas', component: DasboardComponent }  ,
  { path: '', redirectTo: '/home', pathMatch: 'full' },

//   { path: 'home', component: AppComponent,  
//   children: [
//     { path: 'estadisticas', component: CovidComponent, },
//     { path: 'graficas', component: DasboardComponent }          
//   ]
// }
  //{ path: 'normatividad',component: CovidComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
