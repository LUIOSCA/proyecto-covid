import { Component } from '@angular/core';
import { peticionesService } from 'src/services/peticiones.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public informacioncovid: any;

  constructor(private _servicioPeticiones: peticionesService) { }

  ngOnInit() {
  }
}
