import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CovidComponent } from './covid/covid.component';
import { HttpClientModule } from '@angular/common/http';
import { peticionesService } from 'src/services/peticiones.service';
import { DasboardComponent } from './dasboard/dasboard.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
// import { ChartModule } from 'primeng/chart';

@NgModule({
  declarations: [
    AppComponent,
    CovidComponent,
    DasboardComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule
    ],
  providers: [peticionesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
