import { Component, OnInit } from '@angular/core';
import { peticionesService } from 'src/services/peticiones.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-covid',
  templateUrl: './covid.component.html',
  styleUrls: ['./covid.component.css']
})
export class CovidComponent implements OnInit {
  informacioncovid: any;

  constructor(private _servicioPeticiones: peticionesService,private _appComponent: AppComponent) { }

  ngOnInit(): void {
    this.obtenerCovid();
  }

  obtenerCovid() {
    this._servicioPeticiones.getConsultaMetodo('coviddaily').subscribe(
        response => {
           this.informacioncovid = response.result; 
        },
        error => {
            console.log(error.message);
        })
  }

}
