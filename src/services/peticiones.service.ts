import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class peticionesService{
   
   private readonly URLAPI: string = 'http://localhost:3000/';
   constructor(private _http: HttpClient){}

   getConsultaMetodo(metodo: string):Observable<any>{
       return this._http.get(this.URLAPI + metodo);
   }
   
 
 
}
